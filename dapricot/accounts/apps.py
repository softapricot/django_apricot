""" Django Apricot app module
"""
from django.core import checks
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

from dacommon.checks import check_dependencies, AppDependencies


def check_accounts_dependencies(**_):
    """Checks dependencies"""
    dependencies = AppDependencies()
    dependencies.add_dependency("dapricot", 402)
    dependencies.add_dependency("django.contrib.auth", 403)
    dependencies.add_dependency("django.contrib.contenttypes", 404)

    return check_dependencies(
        DjangoApricotConfig.name, DjangoApricotConfig.label, dependencies.deps
    )


class DjangoApricotConfig(AppConfig):
    """App's config"""

    default_auto_field = "django.db.models.AutoField"
    name = "dapricot.accounts"
    label = "daaccounts"
    verbose_name = _("Django Apricot Accounts")
    models_module = "dapricot.accounts.models"

    def ready(self):
        checks.register(check_accounts_dependencies, self.name)
