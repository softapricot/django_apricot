""" Django Apricot Accounts forms module
"""
from django import forms
from django.forms.models import ModelFormOptions
from django.contrib.auth.forms import UserCreationForm

from dapricot.accounts.models import AccountRequest, User


class SignUpForm(UserCreationForm):
    """Signup form"""

    email = forms.EmailField(
        max_length=254, help_text="Required. Inform a valid email address."
    )

    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "password1",
            "password2",
        )


class RequestedSignUpForm(UserCreationForm):
    """Signup form"""

    # TO DO: change username to a dropdown for alternate options.(add alternate option to AccountRequest model)
    email = forms.EmailField(
        max_length=254,
        help_text="Required. Inform a valid email address.",
        widget=forms.TextInput(attrs={"readonly": "readonly"}),
    )
    username = forms.CharField(
        max_length=150,
        help_text="Required. Inform a valid username.",
        widget=forms.TextInput(attrs={"readonly": "readonly"}),
    )

    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "password1",
            "password2",
        )


class RequestUserCreationForm(forms.ModelForm):
    """User request form"""

    class Meta:
        model = AccountRequest
        fields = ("first_name", "last_name", "email")

    def get_available_username(self):
        """gets a valid username for the requester"""
        model = ModelFormOptions(getattr(RequestUserCreationForm, "Meta", None)).model
        username = (
            f"{self.cleaned_data['first_name'][:3]}{self.cleaned_data['last_name'][:3]}"
        )

        for similar_username in (
            model.objects.filter(username__contains=username),
            User.objects.filter(username__contains=username),
        ):
            try:
                similar_username = model.objects.filter(username__contains=username)
                if similar_username.count() > 0:
                    username_complement = similar_username.last().username.split(
                        username
                    )[-1]
                    username_complement = (
                        int(username_complement if username_complement != "" else "1")
                        + 1
                    )
                    username = f"{username}{username_complement}"
                    break
            except model.DoesNotExist:
                pass

        return username

    def save(self, commit=True):
        request = super().save(commit=False)
        request.username = self.get_available_username()
        if commit:
            request.save()
        return request
