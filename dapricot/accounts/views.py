""" Django Apricot Accounts views module
"""
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.contrib.auth import login, get_user_model, views, logout
from django.shortcuts import render, redirect
from django.urls.base import reverse_lazy
from django.conf import settings
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from dacommon.views import BaseView, DjangoStyleOverrideMixin, MessageView
from dapricot.accounts.forms import (
    SignUpForm,
    RequestedSignUpForm,
    RequestUserCreationForm,
)
from dapricot.accounts.tokens import account_activation_token, signup_token
from dapricot.accounts.models import AccountRequest

__all__ = [
    "PasswordResetConfirmView",
    "PasswordResetView",
    "LogoutView",
    "PasswordChangeView",
    "LoginView",
    "SignupView",
    "ActivateView",
]


class AccountView(BaseView):
    """Account dashboard view"""

    template_name = "dapricot/accounts/account.html"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class PasswordResetView(DjangoStyleOverrideMixin, views.PasswordResetView):
    """Password Reset View"""

    template_name = "dapricot/accounts/password_reset_form.html"
    email_template_name = "dapricot/accounts/password_reset_email.html"
    success_url = reverse_lazy("daaccounts:password_reset_done")


class PasswordResetConfirmView(
    DjangoStyleOverrideMixin, views.PasswordResetConfirmView
):
    """Password Reset Confirm View"""

    template_name = "dapricot/accounts/password_reset_confirm.html"
    success_url = reverse_lazy("daaccounts:password_reset_complete")


class LogoutView(MessageView):
    """Password Reset View"""

    message = "Logged out"

    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)


class PasswordChangeView(DjangoStyleOverrideMixin, views.PasswordChangeView):
    """Password Reset View"""

    template_name = "dapricot/accounts/password_change_form.html"
    success_url = reverse_lazy("daaccounts:password_change_done")


class LoginView(DjangoStyleOverrideMixin, views.LoginView):
    """Password Reset View"""

    template_name = "dapricot/accounts/login.html"
    redirect_authenticated_user = True


class SignupView(BaseView):
    """Signup view"""

    template_name = "dapricot/accounts/signup.html"

    def post(self, request, *args, **kwargs):
        """Post request"""
        form = None
        if settings.AUTO_SIGNUP_ENABLED:
            form = SignUpForm(request.POST)
        elif "uidb64" in kwargs:
            uid = urlsafe_base64_decode(kwargs["uidb64"])
            account_request = AccountRequest.objects.get(pk=uid)
            if signup_token.check_token(account_request, kwargs["token"]):
                form = RequestedSignUpForm(request.POST)
        elif settings.ACCOUNTS_REQUESTS_ENABLED:
            form = RequestUserCreationForm(request.POST)

        self.template_variables["form"] = form

        if form is not None:
            if form.is_valid():
                if isinstance(form, RequestUserCreationForm):
                    form.save()

                    return redirect(settings.SIGNUP_REDIRECT_URL)

                user = form.save(commit=False)
                user.is_active = False
                user.save()

                current_site = get_current_site(request)
                subject = "Activate Your MySite Account"
                message = render_to_string(
                    "dapricot/accounts/account_activation_email.html",
                    {
                        "user": user,
                        "domain": current_site.domain,
                        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                        "token": account_activation_token.make_token(user),
                    },
                )
                user.email_user(subject, message)
                if (
                    settings.ACCOUNTS_REQUESTS_ENABLED
                    and not settings.AUTO_SIGNUP_ENABLED
                    and signup_token.check_token(account_request, kwargs["token"])
                ):
                    # TO DO: change delete for 'is_active' field
                    account_request.delete()

                return redirect("daaccounts:account_activation_sent")

            return render(request, self.template_name, self.template_variables)
        # TO DO: change last redirect for an error page
        return redirect("daaccounts:signup")

    def get(self, request, *args, **kwargs):
        """Get request"""
        if request.user.is_authenticated and not request.user.is_staff:
            return redirect("home")

        if settings.AUTO_SIGNUP_ENABLED:
            form = SignUpForm()
        elif settings.ACCOUNTS_REQUESTS_ENABLED:
            form = RequestUserCreationForm()
            if "uidb64" in kwargs:
                uid = urlsafe_base64_decode(kwargs["uidb64"])
                account_request = AccountRequest.objects.get(pk=uid)
                if signup_token.check_token(account_request, kwargs["token"]):
                    data = {
                        "username": account_request.username,
                        "email": account_request.email,
                    }
                    form = RequestedSignUpForm(initial=data)
                else:
                    return redirect("daaccounts:account_request")

        self.template_variables["form"] = form
        return super().get(request)


class ActivateView(BaseView):
    """Signup view"""

    template_name = "account_activation_invalid.html"

    def get(self, request, uidb64, token):
        """Get request"""
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = get_user_model().objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, get_user_model().DoesNotExist):
            user = None

        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.email_confirmed = True
            user.save()
            login(request, user)
            return redirect(settings.LOGIN_REDIRECT_URL)

        return super().get(request)
