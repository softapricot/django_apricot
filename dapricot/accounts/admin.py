""" Daaccounts admin module
"""
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.html import format_html

from dapricot.accounts.models import AccountRequest, User


@admin.register(AccountRequest)
class AccountRequestAdmin(admin.ModelAdmin):
    """Model admin for account requests"""

    list_display = (
        "username",
        "first_name",
        "last_name",
        "email",
        "request_actions",
    )
    readonly_fields = (
        "email",
        "first_name",
        "last_name",
        "request_date",
        "request_actions",
    )
    date_hierarchy = "request_date"

    def request_actions(self, obj):
        """displays button to navigate to signup on approved request"""
        return format_html(
            f'<a class="button" href="{obj.get_account_creation_link()}">Signup</a>'
        )

    request_actions.short_description = ""
    request_actions.allow_tags = True


@admin.register(User)
class AccountAdmin(UserAdmin):
    """Model admin for Users"""
