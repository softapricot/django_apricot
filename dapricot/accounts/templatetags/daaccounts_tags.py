""" Django Apricot accounts tags module
"""
from django import template
from django.conf import settings

register = template.Library()


@register.inclusion_tag("dapricot/tags/collapsible_menu.html", takes_context=True)
def user_menu(context):
    """Returns user menu"""
    request = context["request"]

    links = {}
    if request.user.is_authenticated:
        title_link = [True, "daaccounts:account", "icon-user", request.user.username]
        links["Logout"] = [True, "daaccounts:logout", "icon-logout"]
        if request.user.is_staff:
            links["Admin"] = [True, "admin:index", "icon-key"]
    else:
        title_link = [None, "", "icon-users", "Welcome"]
        links["Login"] = [True, "daaccounts:login", "icon-login"]
        if settings.AUTO_SIGNUP_ENABLED:
            links["Register"] = [True, "daaccounts:signup", "icon-user-add"]
        elif settings.ACCOUNTS_REQUESTS_ENABLED:
            links["Register"] = [True, "daaccounts:account_request", "icon-user-add"]

    return {"links": links, "title_link": title_link}
