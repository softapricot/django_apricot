""" Django Apricot Accounts tokens module
"""
import six
from django.contrib.auth.tokens import PasswordResetTokenGenerator


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    """Token generator for account activation"""

    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk)
            + six.text_type(timestamp)
            + six.text_type(user.email_confirmed)
        )


class SignupTokenGenerator(PasswordResetTokenGenerator):
    """Token generator for signup"""

    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.username)
            + six.text_type(timestamp)
            + six.text_type(user.email)
        )


account_activation_token = AccountActivationTokenGenerator()
signup_token = SignupTokenGenerator()
