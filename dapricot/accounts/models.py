""" Django Apricot Accounts models module
"""
from django.db import models
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.urls.base import reverse
from django.conf import settings

from dapricot.accounts.tokens import signup_token, account_activation_token
from dapricot.accounts.managers import UserManager


class User(AbstractUser):
    """User model"""

    USERNAME_FIELD = settings.AUTH_USERNAME_FIELD
    REQUIRED_FIELDS = (
        [
            "email",
        ]
        if settings.AUTH_USERNAME_FIELD == "username"
        else [
            "username",
        ]
    )
    email = models.EmailField(_("email address"), blank=True, unique=True)
    email_confirmed = models.BooleanField(default=False)

    objects = UserManager()

    class Meta:
        db_table = "dapricot_user"
        verbose_name = "user"
        verbose_name_plural = "users"

    def __str__(self):
        return f"{self.username}"

    def get_account_activation_link(self):
        """Returns the signup url"""
        if not self.email_confirmed:
            kwargs = {
                "uid": urlsafe_base64_encode(force_bytes(self.pk)),
                "token": account_activation_token.make_token(self),
            }

            return reverse("daaccounts:activate", kwargs=kwargs)

        return None


class AccountRequest(models.Model):
    """New account request model"""

    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _("username"),
        max_length=150,
        unique=True,
        validators=[username_validator],
        blank=True,
    )
    request_date = models.DateTimeField(
        _("request_date"), auto_now_add=True, editable=False
    )
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    email = models.EmailField(_("email address"), unique=True, blank=True)

    class Meta:
        db_table = "dapricot_account_request"
        verbose_name = "account request"
        verbose_name_plural = "account requests"

    def __str__(self):
        return f"{self.username}"

    def get_account_creation_link(self):
        """Returns the signup url"""
        kwargs = {
            "uidb64": urlsafe_base64_encode(force_bytes(self.pk)),
            "token": signup_token.make_token(self),
        }

        return reverse("daaccounts:signup", kwargs=kwargs)
