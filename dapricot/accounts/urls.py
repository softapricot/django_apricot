""" Django Apricot Accounts urls module
"""
from django.urls.conf import path, re_path
from django.conf import settings

from dacommon.views import MessageView
from dapricot.accounts.views import (
    LoginView,
    LogoutView,
    PasswordChangeView,
    PasswordResetView,
    PasswordResetConfirmView,
    ActivateView,
    SignupView,
    AccountView,
)

app_name = "daaccounts"

urlpatterns = [
    path("", AccountView.as_view(), name="account"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("password_change/", PasswordChangeView.as_view(), name="password_change"),
    path(
        "password_change/done/",
        MessageView.as_view(
            message_title="Password change",
            message="Password change successful",
        ),
        name="password_change_done",
    ),
    path("password_reset/", PasswordResetView.as_view(), name="password_reset"),
    path(
        "password_confirm/",
        PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "password_reset/done/",
        MessageView.as_view(
            message_title="Password reset",
            message=(
                "We’ve emailed you instructions for setting your password, "
                "if an account exists with the email you entered. You should receive them shortly.",
                "If you don’t receive an email, please make sure you’ve entered the address you registered with, "
                "and check your spam folder.",
            ),
        ),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        MessageView.as_view(
            message_title="Password reset complete",
            message="Your password has been set. You may go ahead and log in now.",
        ),
        name="password_reset_complete",
    ),
    path(
        "account_activation_sent/",
        MessageView.as_view(
            template_name="dapricot/accounts/account_activation_sent.html",
            message=(
                MessageView.MessageType.INFO,
                "Your activation link has been sent.",
            ),
        ),
        name="account_activation_sent",
    ),
    re_path(
        r"activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,6}-[0-9A-Za-z]{1,32})/",
        ActivateView.as_view(),
        name="activate",
    ),
]

if settings.AUTO_SIGNUP_ENABLED:
    urlpatterns += [
        path("signup/", SignupView.as_view(), name="signup"),
    ]

elif settings.ACCOUNTS_REQUESTS_ENABLED:
    urlpatterns += [
        path("signup/", SignupView.as_view(), name="account_request"),
        re_path(
            r"signup/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,6}-[0-9A-Za-z]{1,32})/",
            SignupView.as_view(),
            name="signup",
        ),
    ]
